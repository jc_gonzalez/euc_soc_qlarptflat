#!/bin/bash 
#------------------------------------------------------------------------------------
# QLAReportFlatten.sh
# QLA JSON Report Flattener (converts to CSV)
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2015-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $(which $0)); pwd; cd - > /dev/null)
export PYARES_INI_FILE=/home/ares/.config/aresri/retrieval_config.ini
export PYTHONPATH=${SCRIPTPATH}

echo ""
${PYTHON:=python3} ${SCRIPTPATH}/qla_rpt_flatten/qla_rpt_flatten.py $*
