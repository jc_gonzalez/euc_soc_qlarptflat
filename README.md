Euclid SOC QLA (QDT) Diagnostics Report Flattener
==========================================

This directory and all its sub-directories contain the launcher and modules of the
Euclid SOC QLA Diagnostics Report Flattener.

This tool takes a QDT report file (or a folder containing such files), and
converts them to a pair of CSV files, one with the actual data values, and another
with the set of parameter definitions, both suitable to be ingested by the
HMS ARES tools. 

This is performed for VIS, NISP (NIR and SIR) and AOCS reports.

In addition, this CSV files can be parsed by the ESS.

Installation and Execution
--------------------------

For installing and running this tool, just clone the `git` repository

    $ git clone <euc_soc_qparptflat-repository-url>
    
Then, add the directory where the repository was downloaded to the PATH
environment variable

    $ export PATH=$PATH:<euc_soc_qlarptflat-full-path>

Finally, you can launch the script `QLAReportFlatten.sh` to get a message 
on the valid command line arguments.

License
--------

You should have received a copy of the GNU Lesser General Public License
along with QPF (please, see [COPYING][1] and [COPYING.LESSER][2] for
information on this license).  If not, see <http://www.gnu.org/licenses/>.

[1]: ./COPYING
[2]: ./COPYING.LESSER
