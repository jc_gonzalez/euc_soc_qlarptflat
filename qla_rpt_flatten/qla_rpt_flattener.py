#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
QLA (QDT) Diagnostics Report Flattener

This module defines the class that holds the actual functionality of the
QLA (QDT) Diagnostics Report Flattener, to convert QDT report files to
a pair of CSV files each, one with the actual data values, and another
with the set of parameter definitions, both suitable to be ingested by
the HMS ARES tools.  This is performed for VIS, NISP )NIR and SIR) and
AOCS reports.
"""
#----------------------------------------------------------------------

import os, sys

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

#----------------------------------------------------------------------

import logging
import glob
import json
import math
import shutil

from copy import deepcopy
from pprint import pprint
from tempfile import _get_candidate_names as getTempFileName

logger = logging.getLogger()

from qla_rpt_translations import CCDLevel, DiagnosticLevel, MetricLevel, ValueLevel, \
                                 ArrayItems, InstrumentId, \
                                 VISTranslation, NIRTranslation, SIRTranslation, AOCSTranslation

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

class Struct:
    def __init__(self, *sequential, **named):
        fields = dict(zip(sequential, [None]*len(sequential)), **named)
        self.__dict__.update(fields)
    def __repr__(self):
        return str(self.__dict__)

def isstrnum(s):
    return any(c.isdigit() for c in s)

class ReportFlattener:
    """
    Main class for the conversion of hierarchical JSON QLA report into a flattened version of
    it, in CSV format
    """

    Translation = {}

    TranslationTable = {InstrumentId.VIS: VISTranslation,
                        InstrumentId.NIR: NIRTranslation,
                        InstrumentId.SIR: SIRTranslation,
                        InstrumentId.AOCS: AOCSTranslation}

    def __init__(self, type=None):
        """
        Initialize current instance
        """
        self.initialize()

        if type:
            self.setTranslationTable(instStr=type)
        else:
            self.setTranslationTable(inst=InstrumentId.VIS)

    def initialize(self):
        self.levels = {}
        self.levels_per_file = {}
        self.max_tag_len = 0
        self.metadata : dict = {}
        self.file_csv_content = []
        self.date = ''
        self.valid_levels = [DiagnosticLevel, MetricLevel, ValueLevel]
        self.minlvl = self.valid_levels[0]
        self.instrument = InstrumentId.UNKNOWN

    def setTranslationTable(self, inst=None, instStr=None):
        """
        Set the translation table for a given set of parameters
        :param instStr:
        :return:
        """
        if instStr:
            if not instStr in InstrumentId._member_names_:
                logger.fatal(f'Cannot find translation table for {instStr}')
            inst = InstrumentId._member_map_[instStr]

        self.instrument = inst
        logger.info(f'Translating a report for a product of type {inst}')
        ReportFlattener.Translation = ReportFlattener.TranslationTable[self.instrument]

    def parse_filename(self, s):
        """
        Parse filename (expected to follow the convention EUC_QLA_VIS-obsid-e-m_...
        :param s: The string with the file name
        :return:
        """
        logger.info(f'Parsing "{s}" . . .')
        self.metadata : dict = {'origin': s[4:7],
                         'instrument': s[8:11],
                         'obsid': s[12:17],
                         'exposure': int(s[18:19]),
                         'obsmode': s[20:21],
                         'date': ''}

        self.setTranslationTable(instStr=self.metadata['instrument'])

        self.file_csv_content.clear()
        self.file_csv_content.append(('ORIGIN', self.metadata['origin']))
        self.file_csv_content.append(('INSTRUMENT', self.metadata['instrument']))
        self.file_csv_content.append(('OBS_ID', self.metadata['obsid']))
        self.file_csv_content.append(('EXPOSURE', self.metadata['exposure']))
        self.file_csv_content.append(('OBS_MODE', self.metadata['obsmode']))

    def show_dict(self, d, level):
        """
        Show the dictionary
        :param d: the dictionary being shown
        :param level: The level being shown
        :return: 
        """
        indent = '. ' * level
    
        if not level in self.levels.keys():
            self.levels[level] = []
    
        for k, v in d.items():
            if not k in self.levels[level]:
                self.levels[level].append(k)
            if isinstance(v, dict):
                logger.debug('{} {}:'.format(indent, k))
                self.show_dict(v, level + 1)
            else:
                logger.debug('{} {}: {}'.format(indent, k, v))
    
    def show_report(self, ifile):
        """
        Show report contents
        :param ifile: the input report
        :return: -
        """
        with open(ifile, 'r') as fp:
            rep = json.load(fp)
    
        self.levels.clear()
        self.show_dict(rep, 0)
        self.levels_per_file[ifile] = self.levels

    def translate_ccdLevel(self, k):
        """
        Translate the index in the first mapping level
        :param k: the keyword
        :return: the translation, or None if no translation possible
        """
        if self.instrument == InstrumentId.VIS and k[:3] == 'CCD':
            q = str(int(k[9:10])) if k[8:9] == 'Q' else ''
            self.tagsDict.description[str(CCDLevel)] = k
            return 'Ccd{}{}{}'.format(k[4:5], k[6:7], q)

        if self.instrument in (InstrumentId.NIR, InstrumentId.SIR) and k[:3] == 'DET':
            self.tagsDict.description[str(CCDLevel)] = k
            return 'Det{}{}'.format(k[4:5], k[5:6])

        if self.instrument == InstrumentId.AOCS and k[:1] == 'T':
            self.tagsDict.description[str(CCDLevel)] = k
            return '' #'T{}'.format(k[2:3])

        return None

    def translate_key(self, lvl, k):
        """
        Define the translation of the different elements
        :param lvl: level
        :param k:
        :param td:
        :return: the translation of the given element
        """

        if lvl == CCDLevel:
            source = self.translate_ccdLevel(k)
            if source: return source

        if not lvl in self.valid_levels:
            return ''
    
        self.tagsDict.description[str(lvl)] = k
        for l in range(lvl+1, 9):
            if str(l) in self.tagsDict.description.keys():
                self.tagsDict.description[str(l)] = ''
    
        if lvl == ValueLevel:
            if k[:4] == "Rows" and isstrnum(k[6:10]):
                return 'R{}_{}'.format(k[6:10], k[11:15])
            elif k[:6] == "Column" and isstrnum(k[-4:]):
                return 'C{}'.format(k[-4:])
            elif k[:6] == "Region" and isstrnum(k[8:12]):
                return '_{}_{}'.format(k[8:12], k[13:17])

        if k in ReportFlattener.Translation[str(lvl)]:
            transl = ReportFlattener.Translation[str(lvl)][k]
            if transl in ArrayItems:
                transl = None
        else:
            transl = None

        return transl

    def translate_dict(self, ky, d, level, name):
        """
        Traverse dictionary, and translate it
        :param d: the current item
        :param level: the current level
        :param name: the item node to process
        :return:
        """
        indent = '. ' * level

        #logger.debug(f'Traversing {indent}{name}: {str(d)[:60]}...')
        logger.debug(f'@ {indent}{ky}')

        for k, v in d.items():

            if k in ['name', 'messages']:
                continue

            if k == 'Date':
                self.metadata['date'] = v
                self.date = v
                logger.debug(f'Date: {self.metadata["date"]}')

            if k[:4] == "EUC_":
                self.parse_filename(k)

            t = self.translate_key(level, k)

            if t is None:
                logger.warning(f'Translation of {name}.{k} (level {level}) cannot be done!')
                continue

            if isinstance(v, dict):
                self.translate_dict(k, v, level + 1, name + t)
            elif isinstance(v, list):
                lv = len(v)
                if lv < 1: continue
                digits = int(math.log10(lv) + 1)
                for j, vv in enumerate(v):
                    self.translate_dict(k, vv, level + 1, name + t + f'{j+1:0{digits}d}')
            else:
                tag = name
                if isinstance(v, int) or isinstance(v, float):
                    tag = name + t
                elif k == 'outcome':
                    tag = name + "_result"
                    v = 'OK' if (v == 'Nominal') else 'WARNING'
                line = '{} {}: {}'.format(indent, k, v)
                if k in ['name', 'messages']:
                    logger.debug(line)
                    continue
                self.max_tag_len = max(self.max_tag_len, len(tag))

                self.file_csv_content.append((tag, v))

                if len(tag) > 0:
                    self.tagsDict.tags.append(deepcopy(tag))
                    self.tagsDict.descriptions.append(deepcopy(self.tagsDict.description))
                    self.tagsDict.types.append('DOUBLE' if isinstance(v, float) else
                                    ('INT' if isinstance(v, int) else 'STRING'))

                if len(tag) > 32:
                    tag = '*' + tag

                logger.debug('@@{:80s}   "{}": {}'.format(line, tag, v))
                #print('>>>> {} ----> {}'.format(tag, td.description))

    def translate_report(self, ifile):
        """
        Launches the translation of a given report file
        :param ifile: the input report to translate
        :return: the tags dictionary
        """
        self.initialize()

        with open(ifile, 'r') as fp:
            rep = json.load(fp)

        self.tagsDict = Struct(descriptions=[], types=[], tags=[], description={})
        self.translate_dict(ifile, rep, 0, '')
    
    def list_csv_content(self, ofile):
        """
        List CSV content
        :param ofile: output file
        :return: -
        """
        # Format is:
        # Parameter, Time, Value

        the_date = self.date
    
        tmpfile = os.path.join("/tmp", next(getTempFileName()))
        with open(tmpfile, "w") as fp:
            fp.write('Parameter, Time, Value\n')
            for item in self.file_csv_content:
                fp.write('{}, {}, {}\n'.format(item[0], the_date, item[1]))
        shutil.move(tmpfile, ofile)

    def list_csv_definitions(self, ofile):
        """
        List CSV definitions
        :param ofile: the output file for the definitions
        :return:
        """
        # Format is:
        # Parameter, Type, Description, Unit, Active
        # TESTPARAM002, DOUBLE, This is the last description of the test parameter, Hz, 1

        ccdLevel = str(CCDLevel)
        diagLevel = str(DiagnosticLevel)
        metricLevel = str(MetricLevel)
        valueLevel = str(ValueLevel)

        tmpfile = os.path.join("/tmp", next(getTempFileName()))
        with open(tmpfile, "w") as fp:
            fp.write('Parameter, Type, Description, Unit\n')
            for k in range(0, len(self.tagsDict.tags)):
                tag = self.tagsDict.tags[k]
                d = self.tagsDict.descriptions[k]
                tp = self.tagsDict.types[k]
                diag = d[diagLevel]
                item = d[metricLevel]
                if valueLevel in d.keys():
                    item = item + ', ' + d[valueLevel]
                dccd = '' if ccdLevel not in d else f' for {d[ccdLevel]}'
                desc = "{}, {}{}".format(diag, item, dccd)
                units = ''
                if d[metricLevel][-1:] == ']':
                    rbkt = d[metricLevel].rfind('[')
                    units = '"{}"'.format(d[metricLevel][rbkt+1:-1])
                fp.write('"{}", {}, "{}", {}\n'.format(tag, tp, desc, units))
        shutil.move(tmpfile, ofile)

    def process(self, report_file=None, input_folder=None, output_folder=None):
        """
        Translate a single report file, and/or all the reports found in a given folder
        :param report_file: the report file to process
        :param input_folder: the folder with input reports to process
        :return: -
        """
        if report_file:
            logger.info(f'Processing single file:')
            logger.info(f'- Processing file {report_file} . . .')
            self.translate_report(report_file)

            if not output_folder:
                output_folder = os.path.realpath(os.path.dirname(report_file))
            bfile = os.path.splitext(os.path.basename(report_file))[0]
            logger.info('-- Saving values file . . .')
            self.list_csv_content(f'{output_folder}/{bfile}.csv')
            logger.info('-- Saving definitions file . . .')
            self.list_csv_definitions(f'{output_folder}/{bfile}.def.csv')

        if input_folder:
            logger.info(f'Processing folder {input_folder} :')
            for f in glob.glob(input_folder + "/*.json"):
                self.process(report_file=f)


if __name__ == '__main__':
    pass

