#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Flattening of QLA (QDT) Diagnostics Report

This tool takes a QDT report file (or a folder containing such files), and
converts them to a pair of CSV files, one with the actual data values, and another
with the set of parameter definitions, both suitable to be ingested by the
HMS ARES tools.

This is performed for VIS, NISP (NIR and SIR) and AOCS reports.

In addition, this CSV files can be parsed by the ESS.
"""
#----------------------------------------------------------------------

import os, sys

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

#----------------------------------------------------------------------

import argparse
import logging

logger = logging.getLogger()

from qla_rpt_flattener import ReportFlattener

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

def configureLogs(level):
    logger.setLevel(level)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(level)

    # Create formatters and add it to handlers
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    if 'LOGGING_MODULES' in os.environ:
        for lname in os.environ['LOGGING_MODULES'].split(':'):
            lgr = logging.getLogger(lname)
            if not lgr.handlers: lgr.addHandler(c_handler)


def getArgs():
    """
    Parse arguments from command line

    :return: args structure
    """
    parser = argparse.ArgumentParser(description='QLA Diagnostics Report File Flattener (JSON => CSV)',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input_file', dest='input_file',
                        help='Full path name of the input (JSON) report file')
    parser.add_argument('-I', '--input_dir', dest='input_dir',
                        help='Folder with input report files')
    parser.add_argument('-o', '--output_dir', dest='output_dir',
                        help='Folder to save the output (flattened, CSV) report files')
    parser.add_argument('-t', '--type', dest='type', default=None,
                        help='Explicitly state the type of the report (VIS,NIR,SIR,AOCS)')
    parser.add_argument('-d', '--debug', dest='debug', default=False, action='store_true',
                        help='Show debug information')

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    return parser.parse_args()


def greetings():
    """
    Says hello
    """
    logger.info('=' * 60)
    logger.info('qla-rpt-flatten - QLA Diagnostics Report File Flattener (JSON => CSV)')
    logger.info('=' * 60)


def main():
    """
    Main program
    """
    args = getArgs()
    configureLogs(logging.INFO if not args.debug else logging.DEBUG)
    greetings()

    if not args.input_file and not args.input_dir:
        logger.fatal('Not enough arguments.  You must specify one of --input_file (-i) or --input_dir (-I).')

    rptFlat = ReportFlattener(type=args.type)
    rptFlat.process(report_file=args.input_file,
                    input_folder=args.input_dir,
                    output_folder=args.output_dir)

    logger.info(f'Maximum length of tag: {rptFlat.max_tag_len}')
    logger.info('Done')


if __name__ == '__main__':
    main()

