#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Flattening of QLA (QDT) Diagnostics Report

This module holds the dictionaris to transform from keyword and keyword description
to symbol/acronym to build the final CSV identifier.
"""
#----------------------------------------------------------------------

import os, sys

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

#----------------------------------------------------------------------

from enum import Enum

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

CCDLevel, DiagnosticLevel, MetricLevel, ValueLevel = 2, 4, 6, 7

ArrayItems = ['OFFfault1r', 'ONfault1r', 'NumCRbin'] #, 'HotCols']  #, 'BloomAreas']

class InstrumentId(Enum):
    UNKNOWN=0
    VIS=1
    NIR=2
    SIR=3
    AOCS=4

VISTranslation = {
    str(DiagnosticLevel): {
        'Astrometry': 'Astro',
        'Avg_Dark_Signal': 'AvgDrkSgn',
        'CCD_Consistency': 'CcdCons',
        'CCD_Extrsources': 'CcdExtSrc',
        'Charge_Injection_Level': 'ChgInLvl',
        'Charge_Injection_Pattern': 'ChgInPat',
        'Cosmic_Rays': 'Cosmics',
        'Electronic_Offset': 'EleOff',
        'Electronic_Offset_Systematics': 'EleOffSys',
        'Hot_Columns': 'HotCols',
        'Illumination_Fluence': 'IllumFlu',
        'Overflow_Pixels': 'OvflPix',
        'PRNU': 'PRNU',
        'PSF': 'PSF',
        'Parallel_Cti': 'ParCti',
        'Parallel_Cti_FPR': 'ParCtiFPR',
        'Particulate_Contamination': 'PartCont',
        'Readout_Noise': 'RdoutNois',
        'Saturated_pixels': 'SatPix',
        'Saturation_Level': 'SatLvl',
        'Serial_CTI_Trailing': 'SerCTITrail',
        'Statistics': 'Stats',
        'Underflow_Pixels': 'UnflPix',
        'BIAS_COR': 'BIASCOR',
        'GAIN_COR': 'GAINCOR'
    },
    str(MetricLevel): {
        'outcome': '',
        'messages': 'msgs',
        'Average PRNU [Electrons]': 'AvgPRNU',
        'Average RON [ADU]': 'AvgRON',
        'Average dark signal [Electrons/s]': 'AvgDrkSgn',
        'Average FHWM [arcsec]': 'AvgFWHM',
        'Average fluence [Electrons]': 'AvgFluence',
        'Average of ON blocks averages [ADU]': 'AvgONblkavg',
        'Blooming_areas': 'BloomAreas',
        'CR density [Number of pixels/s]': 'CRdens',
        'CTI ratio': 'CTIratio',
        'Column': 'Col',
        'Column diff Q1-Q2 [Electrons]': 'CDifQ1Q2',
        'Column diff Q3-Q4 [Electrons]': 'CDifQ3Q4',
        'Columns average [Electrons]': 'ColAvg',
        'First row of OFF faulty blocks': 'OFFfault1r',
        'First row of ON faulty blocks': 'ONfault1r',
        'Histogram Maximum [Electrons]': 'HistMax',
        'Histogram Minimum [Electrons]': 'HistMin',
        'hot_columns': 'HotCols',
        'Hot_Columns': 'HotCols',
        'Image average [Electrons]': 'ImgAvg',
        'Image median [Electrons]': 'ImgMed',
        'Image standard deviation [Electrons]': 'ImgStd',
        'Maximum PRNU [Electrons]': 'MaxPRNU',
        'Minimum PRNU [Electrons]': 'MinPRNU',
        'Multiplication factor[Electrons/ADU]': 'MultpFactor',
        'Number of CR per bin': 'NumCRbin',
        'Number of extracted stars': 'NumStars',
        'Number of histogram bins': 'NumBins',
        'Number of saturated pixels': 'NumSatPix',
        'ON blocks averages [ADU]': 'ONblkavg',
        'ON blocks ratios [(max-min)/avg]': 'ONblkrat',
        'Overscan column-binned diff [ADU]': 'OvscanCDif',
        'Overscan row-binned diff [ADU]': 'OvscanRDif',
        'Parallel CTI': '',
        'Charge loss first block (%)': 'ChgLoss1stBlk',
        'Charge loss remaining blocks (%)': 'ChgLossRemBlks',
        'Droop': 'Drop',
        'PRNU standard deviation [Electrons]': 'StdPRNU',
        'Peak-to-valley fluence [Electrons]': 'PkValley',
        'Plane fitting coefficients': 'PlaneCoef',
        'Pre-Overscan diff [ADU]': 'PreOvscDif',
        'Prescan column-binned diff [ADU]': 'PrescanCDif',
        'Prescan row-binned diff [ADU]': 'PrescanRDif',
        'Region 1 Average [ADU]': 'Reg1Avg',
        'Region 2 Average [ADU]': 'Reg2Avg',
        "Rotation (arcsec)": 'Rot',
        'Shift DEC (arcsec)': 'ShiftDEC',
        'Shift RA (arcsec)': 'ShiftRA',
        'Std deviation FHWM [arcsec]': 'StDevFWHM',
        'Row diff Q1-Q3 [Electrons]': 'RDifQ1Q3',
        'Row diff Q2-Q4 [Electrons]': 'RDifQ2Q4',
        'Single regions values [Electrons]': 'SnglRegVal',
        'Standard deviation of ON blocks averages [ADU]': 'StdONblkavg',
        'Subtracted constant[ADU]': 'SubsConst',
        'Total Average [ADU]': 'TotAvg',
        'Total low response pixels': 'LowRspPix',
        'Total number overscanX': 'TotOvscanX',
        'Total number overscanY': 'TotOvscanY',
        'Total number prescan': 'TotPrescan',
        'Total number science': 'TotNumSci',
        'Total valid rows': 'TotValRows'
    },
    str(ValueLevel): {
        'Column': 'Col',
        'Max level [Electrons]': 'MaxLvl',
        'Median level [Electrons]': 'MedLvl',
        'Number of saturated pixels': 'NumSatPix',
        'Row': 'Row',
        'Row 1': '_1',
        'Row 2': '_2',
        'Row 3': '_3',
        'Avg_remaining_blocks: ': 'AvgRemBlks',
        'First block: ': '1stBlk',
        'Surface offset': 'SrfOff',
        'X tilt': 'Xtilt',
        'Y tilt': 'Ytilt'
    }
}

NIRTranslation = {
    str(DiagnosticLevel): {
        'Astrometry': 'Astrometry',
        'DarkSubtraction_Diag': 'DarkSub',
        'GAIN_COR': 'GAIN_COR',
        'IdentifyCosmicRays_Diag': 'IdentifyCRs',
        'LinearityCorrection_Diag': 'LinCorr',
        'MaskSaturation_Diag': 'MaskSat',
        'PSF': 'PSF',
        'Source_Extraction': 'SrcExtract',
        'Statistics': 'Stats',
        'Statistics_raw': 'StatsRaw'
    },
    str(MetricLevel): {
        'outcome': '',
        'messages': 'msgs',
        'average': 'Avg',
        'Average FHWM [arcsec]': 'AvgFWHM',
        'average_non-cr': 'AvgNonCR',
        'count': 'Count',
        'count_cr_pixels': 'CountCRpix',
        'five_percent': '5perc',
        'median': 'Median',
        'median_non-cr': 'MedianNonCR',
        'Multiplication factor[ADU]': 'MultFactor',
        'Number of extracted stars': 'NumExtractedStars',
        'one_percent': '1perc',
        'percent': 'perc',
        'Rotation (arcsec)': 'Rotation',
        'Shift DEC (arcsec)': 'ShiftDEC',
        'Shift RA (arcsec)': 'ShiftRA',
        'std': 'Std',
        'Std deviation FHWM [arcsec]': 'StdFWHM',
        'std_non-cr': 'StdNonCR',
        'two_percent': '2perc'
    },
    str(ValueLevel): {
    }
}

SIRTranslation = {
    str(DiagnosticLevel): {
        'GAIN_COR': 'GAIN_COR',
        'IdentifyCosmicRays_Diag': 'IdentifyCRs',
        'MaskSaturation_Diag': 'MaskSat',
        'SpectralPositionAngle_Diag': 'SpectralPosAng',
        'SpectralSourceExtraction_Diag': 'SpectralSrcExtract',
        'Statistics': 'Stats',
        'Statistics_raw': 'StatsRaw'
    },
    str(MetricLevel): {
        'outcome': '',
        'messages': 'msgs',
        'abs_delta_avg_theta': 'AbsDeltaAvgTheta',
        'average': 'Avg',
        'average_non-cr': 'AvgNonCR',
        'avg_theta_image': 'AvgThetaImg',
        'count': 'Count',
        'count_cr_pixels': 'CountCRpix',
        'delta_avg_theta': 'DeltaAvgTheta',
        'five_percent': '5perc',
        'median': 'Median',
        'median_non-cr': 'MedianNonCR',
        'Multiplication factor[ADU]': 'MultFactor',
        'Number of extracted spectrums': 'NumExtractedSpecs',
        'one_percent': '1perc',
        'percent': 'perc',
        'std': 'Std',
        'std_non-cr': 'StdNonCR',
        'two_percent': '2perc'
    },
    str(ValueLevel): {
    }
}

AOCSTranslation = {
    str(DiagnosticLevel): {
        'Ang_rates': 'AngRates',
        'Jitter': 'Jitter',
        'Power Spectrum': 'PowerSpec',
        'PSF analysis': 'PSF',
        'Radial pointing': 'RadPoint'
    },
    str(MetricLevel): {
        'outcome': '',
        'messages': 'msgs',
        'Jitter X angular rate percentile [%]': 'JitXperc',
        'Jitter Y angular rate percentile [%]': 'JitYperc',
        'Jitter Z angular rate percentile [%]': 'JitZperc',
        'Jitter X percentile [%]': 'Xperc',
        'Jitter X standard deviation [arcseconds]': 'Xstd',
        'Jitter Y percentile [%]': 'Yperc',
        'Jitter Y standard deviation [arcseconds]': 'Ystd',
        'Jitter Z percentile [%]': 'Zperc',
        'Jitter Z standard deviation [arcseconds]': 'Zstd',
        'JitterX': 'JitX',
        'JitterY': 'JitY',
        'Radial Error': 'RadialErr',
        'Ellipticity variation': 'EllipticityVar',
        'mean': 'Mean',
        'median': 'Median',
        'std': 'Std'
    },
    str(ValueLevel): {
        'Coeff_1': 'Coeff1',
        'Coeff_2': 'Coeff2',
        'Coeff_3': 'Coeff3',
        'Coeff_4': 'Coeff4',
        'Point F1': 'F1',
        'Point F2': 'F2',
        'Point F3': 'F3',
        'Point F4': 'F4',
        'Point F5': 'F5',
        'Point F6': 'F6',
        'Point F7': 'F7',
        'Point F8': 'F8',
        'Point F9': 'F9'
    }
}


if __name__ == '__main__':
    pass

